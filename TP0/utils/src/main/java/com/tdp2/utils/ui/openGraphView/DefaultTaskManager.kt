package com.tdp2.utils.ui.openGraphView

import com.tdp2.utils.ui.openGraphView.tasks.BaseTask
import java.util.concurrent.ExecutorService
import java.util.concurrent.SynchronousQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit
import kotlin.math.max

object DefaultTaskManager {

    private val executor: ExecutorService =
        ThreadPoolExecutor(
            max(Runtime.getRuntime().availableProcessors(), 4),
            Int.MAX_VALUE,
            60L,
            TimeUnit.SECONDS,
            SynchronousQueue()
        )
    private lateinit var currentTask: BaseTask<*>

    fun execute(task: BaseTask<*>) {
        currentTask = task
        executor.execute(task)
    }
}