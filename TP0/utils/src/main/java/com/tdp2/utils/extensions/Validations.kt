package com.tdp2.utils.extensions

import android.util.Patterns
import java.util.regex.Pattern

@Suppress("unused")
fun String.isNameValid(): Boolean {
    return this.length in 2..25 && Pattern.matches("^[a-zA-Z]+\$", this)
}

@Suppress("unused")
fun String.isEmailValid(): Boolean {
    return Patterns.EMAIL_ADDRESS.matcher(this).matches()
}

@Suppress("unused")
fun String.isCodeValid(cardNumber: String): Boolean {

    return when (cardNumber.isCardAmex()) {
        true -> this.length == 4
        false -> this.length == 3
    }
}

@Suppress("unused")
fun String.isCardAmex(): Boolean {
    return this.startsWith("34") || this.startsWith("37")
}

@Suppress("unused")
fun String.isCardNumberValid(): Boolean {
    return this.length in 12..16
}

@Suppress("unused")
fun String.isCouponValid(): Boolean {
    return this.length in 4..25
}

@Suppress("unused")
fun String.isExpDateValid(): Boolean {
    return Pattern.matches("(?:0[1-9]|1[0-2])/[0-9]{2}", this)
}

@Suppress("unused")
fun String.isPasswordValid(): Boolean {
    return this.length >= 6
}

@Suppress("unused")
fun String.isPhoneNumberValid(): Boolean {
    return this.isNotEmpty() && Patterns.PHONE.matcher(this).matches()
}

@Suppress("unused")
fun String.isFullNameValid(): Boolean {
    return Pattern.matches("^[a-zA-Z]+\$", this)
}

@Suppress("unused")
fun String.isFirstNameValid(): Boolean {
    return this.isNotEmpty() && Pattern.matches("^[a-zA-Z]+\$", this)
}

@Suppress("unused")
fun String.isLastNameValid(): Boolean {
    return this.isNotEmpty() && Pattern.matches("^[a-zA-Z]+\$", this)
}

@Suppress("unused")
fun String.isConfirmPasswordValid(password: String): Boolean {
    return password == this
}
