package com.tdp2.utils.ui.placepicker

import android.content.Context
import android.location.Address
import android.text.TextUtils
import android.util.Log
import androidx.annotation.Nullable
import com.tdp2.utils.R
import com.tdp2.utils.extensions.append
import com.google.android.gms.maps.model.LatLng
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.MalformedURLException
import java.net.URLEncoder
import java.util.*
import kotlin.collections.ArrayList

interface OnPlacesDetailsListener {
    fun onPlaceFetched(place: Place)
    fun onError(errorMessage: String)
}

interface OnAddressListener {
    fun onAddressFetched(address: Address)
    fun onError(errorMessage: String)
}

class PlaceAPI private constructor(
    private var apiKey: String?, private var sessionToken: String?, private var appContext: Context
) {
    /**
     * Used to get details for the places api to be showed in the auto complete list
     */
    internal fun autocomplete(input: String): ArrayList<Place>? {
        checkInitialization()
        val resultList: ArrayList<Place>? = null
        var conn: HttpURLConnection? = null
        val jsonResults = StringBuilder()
        try {
            val sb = buildApiUrl(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON)
            sb.append("&input=" + URLEncoder.encode(input, "utf8"))
            val url = java.net.URL(sb.toString())
            conn = url.openConnection() as HttpURLConnection
            val inputStreamReader = InputStreamReader(conn.inputStream)
            constructData(inputStreamReader, jsonResults)
        } catch (e: Exception) {
            when (e) {
                is MalformedURLException -> logError(e, R.string.error_processing_places_api)
                is IOException -> logError(e, R.string.error_connecting_to_places_api)
            }
            return resultList
        } finally {
            conn?.disconnect()
        }
        return parseAutoCompleteData(jsonResults)
    }

    /**
     * Fetches the details of the place
     */
    @Nullable
    fun fetchPlaceDetails(place: Place, listener: OnPlacesDetailsListener) {
        checkInitialization()
        Thread(Runnable {
            var conn: HttpURLConnection? = null
            val jsonResults = StringBuilder()
            try {
                val sb = buildApiUrl(PLACES_API_BASE + TYPE_DETAIL + OUT_JSON)
                sb.append("$PARAM_PLACE_ID${place.id}")
                conn = java.net.URL(sb.toString()).openConnection() as? HttpURLConnection
                conn?.apply {
                    val inputStreamReader = InputStreamReader(inputStream)
                    constructData(inputStreamReader, jsonResults)
                    parseDetailsData(place, jsonResults, listener)
                }
            } catch (e: Exception) {
                when (e) {
                    is JSONException -> parseDetailsError(jsonResults, listener, e)
                    is MalformedURLException -> showDetailsError(
                        R.string.error_processing_places_api, listener, e
                    )
                    is IOException -> showDetailsError(R.string.error_connecting_to_places_api, listener, e)
                }
            } finally {
                conn?.disconnect()
            }
        }).start()
    }

    @Nullable
    fun getFromLocation(latitude: Double, longitude:Double,listener: OnAddressListener){
        val retList: MutableList<Address> = ArrayList()
        checkInitialization()
        val maxResults = 1
        Thread(Runnable {
            var conn: HttpURLConnection? = null
            val jsonResults = StringBuilder()
            try {
                val url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=$latitude,$longitude&sensor=false&language=es&key=$apiKey"
                conn = java.net.URL(url).openConnection() as? HttpURLConnection
                conn?.apply {
                    val inputStreamReader = InputStreamReader(inputStream)
                    constructData(inputStreamReader, jsonResults)
                }

                val jsonObject = JSONObject(jsonResults.toString())
                if ("OK".equals(jsonObject.getString("status"), ignoreCase = true)) {
                    val results = jsonObject.getJSONArray("results")
                    if (results.length() > 0) {
                        var i = 0
                        while (i < results.length() && i < maxResults) {
                            val result = results.getJSONObject(i)
                            //Log.e(MyGeocoder.class.getName(), result.toString());
                            val addr = Address(Locale.getDefault())
                            // addr.setAddressLine(0, result.getString("formatted_address"));

                            val components = result.getJSONArray("address_components")
                            var streetNumber = ""
                            var route = ""
                            var postalCode = ""
                            var country = ""
                            var adminArea = ""
                            var subLocality = ""

                            for (a in 0 until components.length()) {
                                val component = components.getJSONObject(a)
                                val types = component.getJSONArray("types")
                                for (j in 0 until types.length()) {
                                    when(types.getString(j)){
                                        "locality" -> addr.locality = component.getString("long_name")
                                        "street_number" -> streetNumber = component.getString("long_name")
                                        "route" -> route = component.getString("long_name")
                                        "postal_code" -> postalCode = component.getString("long_name")
                                        "country" -> country = component.getString("long_name")
                                        "administrative_area_level_1" -> adminArea = component.getString("long_name")
                                        "administrative_area_level_2" -> subLocality = component.getString("long_name")
                                    }
                                }
                            }

                            addr.featureName = "$streetNumber $route"
                            addr.subLocality = subLocality
                            addr.adminArea = adminArea
                            addr.countryName = country
                            addr.postalCode = postalCode
                            addr.thoroughfare = ""

                            addr.setAddressLine(0, "$route $streetNumber")
                            addr.latitude = result.getJSONObject("geometry").getJSONObject("location").getDouble("lat")
                            addr.longitude = result.getJSONObject("geometry").getJSONObject("location").getDouble("lng")
                            retList.add(addr)
                            i++
                        }

                        if(retList.size > 0){
                            listener.onAddressFetched(retList[0])
                        }else{
                            listener.onError("Service not available")
                        }
                    }
                }

                Log.e("FIN","JSON")
            } catch (e: Exception) {

            } finally {
                conn?.disconnect()
            }
        }).start()
    }

    private fun checkInitialization() {
        if (TextUtils.isEmpty(apiKey)) {
            throw Exception(appContext.getString(R.string.error_lib_not_initialized))
        }
    }

    private fun buildApiUrl(apiUrl: String): StringBuilder {
        val sb = StringBuilder(apiUrl)
        sb.append("?key=$apiKey")
        if (!TextUtils.isEmpty(sessionToken)) {
            sb.append("&sessiontoken=$sessionToken")
        }
        return sb
    }

    private fun logError(e: Exception, resource: Int) {
        Log.e(TAG, appContext.getString(resource), e)
    }

    private fun parseAutoCompleteData(jsonResults: StringBuilder): ArrayList<Place>? {
        var resultList: ArrayList<Place>? = ArrayList()
        try {
            val jsonObj = JSONObject(jsonResults.toString())
            val predsJsonArray = jsonObj.getJSONArray("predictions")
            resultList = ArrayList(predsJsonArray.length())
            for (i in 0 until predsJsonArray.length()) {
                resultList.add(
                    Place(
                        predsJsonArray.getJSONObject(i).getString("place_id"),
                        predsJsonArray.getJSONObject(i).getString("description")
                    )
                )
            }
            return resultList
        } catch (e: JSONException) {
            val errorJson = JSONObject(jsonResults.toString())
            when {
                errorJson.has(ERROR_MESSAGE) -> Log.e(TAG, errorJson.getString(ERROR_MESSAGE))
                else -> Log.e(TAG, appContext.getString(R.string.error_cannot_process_json_results), e)
            }
            return resultList
        }
    }

    private fun constructData(inputStreamReader: InputStreamReader, jsonResults: StringBuilder) {
        var read: Int
        val buff = CharArray(1024)
        loop@ do {
            read = inputStreamReader.read(buff)
            when {
                read != -1 -> jsonResults.append(buff, 0, read)
                else -> break@loop
            }
        } while (true)
    }

    private fun showDetailsError(resource: Int, listener: OnPlacesDetailsListener, e: Exception) {
        logError(e, resource)
        appContext.getString(resource).let { listener.onError(it) }
    }

    private fun parseDetailsError(
        jsonResults: StringBuilder, listener: OnPlacesDetailsListener, e: Exception
    ) {
        val errorJson = JSONObject(jsonResults.toString())
        if (errorJson.has(ERROR_MESSAGE)) {
            Log.e(TAG, errorJson.getString(ERROR_MESSAGE), e)
            listener.onError(errorJson.getString(ERROR_MESSAGE))
        } else {
            Log.e(TAG, appContext.getString(R.string.error_cannot_process_json_results), e)
            appContext.getString(R.string.error_cannot_process_json_results).let { listener.onError(it) }
        }
    }

    private fun parseDetailsData(place: Place, jsonResults: StringBuilder, listener: OnPlacesDetailsListener) {
        try {
            val jsonObj = JSONObject(jsonResults.toString())
            val resultJsonObject = jsonObj.getJSONObject(RESULT)
            val geometry = resultJsonObject.getJSONObject(GEOMETRY)
            val location = geometry.getJSONObject(LOCATION)

            val coord = LatLng(location.getDouble(LAT), location.getDouble(LNG))
            place.setCoordinate(coord)

            var address: String? = null
            var streetNumber: String? = null

            val jsonArray = resultJsonObject.getJSONArray("address_components")
            for (i in 0 until jsonArray.length()) {
                val component = jsonArray.getJSONObject(i)
                val typeJsonArray = component.getJSONArray("types")
                var locality: String? = null
                var state2: String? = null
                for (j in 0 until typeJsonArray.length()) {
                    val str = typeJsonArray.getString(j)
                    if (!str.isNullOrEmpty()) {

                        when (str) {
                            "locality" -> locality = component.getString("long_name")
                            "street_number" -> streetNumber = component.getString("short_name")
                            "route" -> address = component.getString("short_name")
                            "sublocality_level_1" -> place.city = component.getString("short_name")
                            "administrative_area_level_1" -> place.state = component.getString("long_name")
                            "administrative_area_level_2" -> state2 = component.getString("long_name")
                            "country" -> place.country = component.getString("long_name")
                            "postal_code" -> place.zipcode = component.getString("short_name")
                        }
                    }
                }

                if(place.city.isNullOrEmpty() && locality != null){
                    place.city = locality
                }

                if(place.state.isNullOrEmpty() && state2 != null){
                    place.state = state2
                }
            }
            place.address = streetNumber.append(address, " ")

        } catch (e: java.lang.Exception) {
            return
        }

        listener.onPlaceFetched(place)
    }

    companion object {
        private val TAG = PlaceAPI::class.java.simpleName
        private const val PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place"
        private const val TYPE_AUTOCOMPLETE = "/autocomplete"
        private const val TYPE_DETAIL = "/details"
        private const val PARAM_PLACE_ID = "&placeid="
        private const val OUT_JSON = "/json"
        private const val GEOMETRY = "geometry"
        private const val LOCATION = "location"
        private const val LAT = "lat"
        private const val LNG = "lng"
        private const val RESULT = "result"
        private const val ERROR_MESSAGE = "error_message"
    }

    /**
     * The data class used as builder to allow the user to use different configs of the places API
     */
    data class Builder(
        private var apiKey: String? = null,
        private var sessionToken: String? = null
    ) {
        /**
         * Sets the api key for the PlaceAPI
         */
        fun apiKey(apiKey: String) = apply { this.apiKey = apiKey }

        /**
         * Sets a unique session token for billing in the PlaceAPI
         */
        @Suppress("unused")
        fun sessionToken(sessionToken: String) = apply { this.sessionToken = sessionToken }

        /**
         * Builds and creates an object of the PlaceAPI
         */
        fun build(context: Context) = PlaceAPI(apiKey, sessionToken, context)
    }
}