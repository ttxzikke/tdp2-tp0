package com.tdp2.utils.ui.decorator

import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView

@Suppress("unused")
class VerticalSpaceItemDecoration(private val verticalSpaceHeight: Int = 0) :
    RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {

        val itemPosition = parent.getChildAdapterPosition(view)
        if(itemPosition == 0)
            outRect.top = verticalSpaceHeight

        outRect.bottom = verticalSpaceHeight
        outRect.right = 0
        outRect.left = 0
    }
}