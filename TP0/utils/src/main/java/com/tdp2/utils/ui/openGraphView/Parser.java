package com.tdp2.utils.ui.openGraphView;

import com.tdp2.utils.ui.openGraphView.model.OGData;
import java.io.IOException;
import java.io.InputStream;

public interface Parser {
    OGData parse(InputStream inputStream) throws IOException;
}
