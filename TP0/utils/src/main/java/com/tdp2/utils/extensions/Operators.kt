package com.tdp2.utils.extensions

@Suppress("unused")
infix fun Any?.ifNull(block: () -> Unit) {
    if (this == null) block()
}

