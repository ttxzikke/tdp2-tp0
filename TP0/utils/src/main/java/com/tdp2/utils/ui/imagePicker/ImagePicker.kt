package com.tdp2.utils.ui.imagePicker

import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.fragment.app.Fragment
import com.tdp2.utils.R
import com.tdp2.utils.extensions.*
import com.tdp2.utils.ui.spinner.SpinnerDialog
import com.yalantis.ucrop.UCrop
import java.io.File
import java.util.*

class ImagePicker {

    companion object {
        private const val CAMERA_PERMISSION_REQUEST_CODE = 500
        private const val REQUEST_TAKE_PHOTO = 501
        private const val REQUEST_IMAGE_GALLERY = 502
    }

    private var param = ImagePickerParam()
    private var mPhotoFile: File? = null
    private var mShouldShowPicker = false

    private constructor(param: ImagePickerParam) {
        this.param = param
    }

    @Suppress("UNCHECKED_CAST")
    fun showImagePicker() {
        mShouldShowPicker = true

        if (!checkPermissionForCamera()) {
            requestPermissionForCamera()
            return
        }


        param.activity?.apply {

            val list = if (isCameraAvailable()) listOf(
                Triple(getString(param.galleryOptionTitle), false, 0),
                Triple(getString(param.cameraOptionTitle), false, 1)
            )
            else listOf(
                Triple(getString(param.galleryOptionTitle), false, 0)
            )

            SpinnerDialog.Builder<Int>(this)
                .setTitle(param.title)
                .setOnDismissListener(DialogInterface.OnDismissListener { dialog ->
                    val selections = (dialog as SpinnerDialog<Int>).selectedItems
                    if (selections.isNotEmpty()) {
                        when (selections.first()) {
                            0 -> onPhotoGalleryAction()
                            1 -> onPhotoCameraAction()
                        }
                    }
                })

                .singleSelection(list)
                .setDismissAfterSelect(true)
                .create().show()

//            val options = if (isCameraAvailable()) arrayOf<CharSequence>(
//                getString(param.galleryOptionTitle),
//                getString(param.cameraOptionTitle)
//            ) else arrayOf<CharSequence>(getString(param.galleryOptionTitle))
//
//
//            val builder = AlertDialog.Builder(this, R.style.DialogTheme1)
//            builder.setTitle(getString(param.title))
//            builder.setItems(options) { dialog, which ->
//                when (which) {
//                    0 -> onPhotoGalleryAction()
//                    1 -> onPhotoCameraAction()
//                }
//            }
//            builder.create().show()
        }
    }

    fun showPhotoGallery() {

        if (!checkPermissionForCamera()) {
            requestPermissionForCamera()
            return
        }
        onPhotoGalleryAction()
    }

    private fun onPhotoGalleryAction() {
        val galleryIntent = Intent(Intent.ACTION_GET_CONTENT)
        galleryIntent.type = "image/*"
        param.activity?.apply {
            val chooser = Intent.createChooser(
                galleryIntent,
                resources.getString(R.string.image_source_title)
            )
            startActivity(chooser, REQUEST_IMAGE_GALLERY)
        }
    }

    fun onPhotoCameraAction() {
        mShouldShowPicker = false

        if (!checkPermissionForCamera()) {
            requestPermissionForCamera()
            return
        }

        param.activity?.apply {

            if (isCameraAvailable()) {

                if (!isExternalStorageReadableAndWritable()) {
                    showToast(R.string.sd_not_available)
                }

                try {
                    mPhotoFile = getOutputPhotoFile(this)?.also {
                        val photoURI =
                            FileProvider.getUriForFile(this, param.authority + ".provider", it)
                        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)

                        if (param.startFrontCamera) {
                            takePictureIntent.putExtra(
                                "android.intent.extras.CAMERA_FACING",
                                android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT
                            )
                            takePictureIntent.putExtra("android.intent.extras.LENS_FACING_FRONT", 1)
                            takePictureIntent.putExtra(
                                "android.intent.extra.USE_FRONT_CAMERA",
                                true
                            )
                        }

                        startActivity(takePictureIntent, REQUEST_TAKE_PHOTO)
                    }

                } catch (ex: Exception) {
                    Toast.makeText(this, ex.message, Toast.LENGTH_SHORT).show()
                }


            } else {
                showToast(R.string.camera_not_available)
            }
        }

    }

    private fun createCrop(inputFileUri: Uri, context: Context): UCrop {
        val timeStamp = Calendar.getInstance().timeInMillis
        val imageFileName = "JPEG_$timeStamp"
        val outputFileUri = Uri.fromFile(File(context.cacheDir, imageFileName))
        val crop = UCrop.of(inputFileUri, outputFileUri)
            .withAspectRatio(param.aspectRatioX, param.aspectRatioY)
            .withMaxResultSize(param.maxWidth, param.maxHeight)

        val options = UCrop.Options()
        options.setHideBottomControls(true)
        options.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary))
        options.setStatusBarColor(ContextCompat.getColor(context, R.color.colorPrimary))
        options.setActiveWidgetColor(ContextCompat.getColor(context, R.color.colorWhite))
        options.setToolbarWidgetColor(ContextCompat.getColor(context, R.color.colorWhite))
        crop.withOptions(options)
        options.setToolbarTitle(context.getString(R.string.edit_photo))
        return crop
    }

    private fun showCropImage(inputFileUri: Uri) {
        try {
            if (param.appCompatActivity != null) {
                param.appCompatActivity?.apply {
                    val crop = createCrop(inputFileUri, this)
                    crop.start(this)
                }
            } else if (param.fragment != null) {
                param.activity?.apply {
                    val crop = createCrop(inputFileUri, this)
                    crop.start(this, param.fragment!!)
                }
            }
        } catch (ex: Exception) {
            Toast.makeText(param.activity, ex.message, Toast.LENGTH_SHORT).show()
        }
    }

    private fun didSelectImage() {
        try {
            if (param.compressImage) {
                mPhotoFile?.apply {
                    compressImage(
                        cameraPhotoOrientation(),
                        param.maxWidth,
                        param.maxHeight,
                        param.quality
                    )
                }
            }
        } catch (e: Exception) {
        }

        if (param.cropImage) {
            val outputFileUri = Uri.fromFile(mPhotoFile)
            showCropImage(outputFileUri)
        } else {
            param.listener?.invoke(mPhotoFile)
        }
    }

    fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        when (requestCode) {
            REQUEST_TAKE_PHOTO -> if (resultCode == Activity.RESULT_OK) {
                didSelectImage()
            }

            REQUEST_IMAGE_GALLERY -> if (resultCode == Activity.RESULT_OK) {
                val selectedImage = data!!.data
                param.activity?.apply {
                    mPhotoFile = selectedImage?.createTempFile(this)
                    didSelectImage()
                }
            }

            UCrop.REQUEST_CROP -> {

                if (data == null) {
                    param.listener?.invoke(null)
                    return
                }

                val resultUri = UCrop.getOutput(data)
                if (resultUri != null) {
                    param.activity?.apply {
                        mPhotoFile = resultUri.createTempFile(this)
                        param.listener?.invoke(mPhotoFile)
                    }
                } else {
                    param.listener?.invoke(null)
                }
            }

            UCrop.RESULT_ERROR -> param.listener?.invoke(null)

            else -> {
            }
        }
    }

    fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
                if (mShouldShowPicker) {
                    showImagePicker()
                } else {
                    onPhotoCameraAction()
                }
            }
        }
    }

    private fun requestPermissionForCamera() {
        if (param.fragment != null) {
            param.fragment?.requestPermissionForCamera(CAMERA_PERMISSION_REQUEST_CODE)
        } else {
            param.activity?.requestPermissionForCamera(CAMERA_PERMISSION_REQUEST_CODE)
        }
    }

    private fun checkPermissionForCamera(): Boolean {
        return param.activity?.checkPermissionForCamera() ?: false
    }

    private fun showToast(@StringRes resource: Int) {
        param.activity?.apply {
            Toast.makeText(this, getString(resource), Toast.LENGTH_SHORT).show()
        }
    }

    private fun startActivity(intent: Intent, code: Int) {
        if (param.fragment != null)
            param.fragment?.startActivityForResult(intent, code)
        else
            param.activity?.startActivityForResult(intent, code)
    }

    private fun isExternalStorageReadableAndWritable(): Boolean {
        val externalStorageReadable: Boolean
        val externalStorageWritable: Boolean

        val state = Environment.getExternalStorageState()
        if (state == Environment.MEDIA_MOUNTED) {
            externalStorageWritable = true
            externalStorageReadable = externalStorageWritable
        } else if (state == Environment.MEDIA_MOUNTED || state == Environment.MEDIA_MOUNTED_READ_ONLY) {
            externalStorageReadable = true
            externalStorageWritable = false
        } else {
            externalStorageWritable = false
            externalStorageReadable = externalStorageWritable
        }

        return externalStorageReadable && externalStorageWritable
    }

    private fun getOutputPhotoFile(context: Context): File? {
        val timeStamp = Calendar.getInstance().timeInMillis
        val imageFileName = "JPEG_$timeStamp"
        val storageDir = param.activity?.getExternalFilesDir(Environment.DIRECTORY_PICTURES)

        return try {
            File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
            )
        } catch (e: Exception) {
            File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                context.cacheDir      /* directory */
            )
        }
    }


    private class ImagePickerParam {
        lateinit var authority: String
        var fragment: Fragment? = null
        var appCompatActivity: AppCompatActivity? = null
        var activity: Activity? = null

        @StringRes
        var title: Int = R.string.picker_image_source_title
        @StringRes
        var galleryOptionTitle = R.string.picker_image_source_gallery_option
        @StringRes
        var cameraOptionTitle = R.string.picker_image_source_camera_option

        var listener: ((File?) -> Unit)? = null
        var startFrontCamera = false
        var cropImage = false
        var compressImage = false

        var aspectRatioX = 1.0f
        var aspectRatioY = 1.0f
        var maxWidth = 720
        var maxHeight = 1080
        var quality = 75

    }

    @Suppress("unused")
    class Builder {
        private var param = ImagePickerParam()

        constructor(activity: AppCompatActivity, authority: String) {
            param.appCompatActivity = activity
            param.authority = authority
            param.activity = activity
        }

        constructor(fragment: Fragment, authority: String) {
            param.fragment = fragment
            param.authority = authority
            param.activity = fragment.activity
        }

        fun setTitle(@StringRes textId: Int): Builder =
            apply { param.title = textId }

        fun setGalleryOptionTitle(@StringRes textId: Int): Builder =
            apply { param.galleryOptionTitle = textId }

        fun setCameraOptionTitle(@StringRes textId: Int): Builder =
            apply { param.cameraOptionTitle = textId }

        fun setListener(listener: ((File?) -> Unit)?): Builder {
            param.listener = listener
            return this
        }

        fun setStartFrontCamera(startFrontCamera: Boolean): Builder =
            apply { param.startFrontCamera = startFrontCamera }

        fun setCropImage(
            maxWidth: Int = 720,
            maxHeight: Int = 1080,
            aspectRatioX: Float = 1.0f,
            aspectRatioY: Float = 1.0f
        ): Builder =
            apply {
                param.cropImage = true
                param.maxWidth = maxWidth
                param.maxHeight = maxHeight
                param.aspectRatioX = aspectRatioX
                param.aspectRatioY = aspectRatioY
            }

        fun setCompressImage(
            maxWidth: Int = 720,
            maxHeight: Int = 1080,
            quality: Int = 75
        ): Builder =
            apply {
                param.compressImage = true
                param.maxWidth = maxWidth
                param.maxHeight = maxHeight
                param.quality = quality
            }

        fun create(): ImagePicker {
            return ImagePicker(param)
        }

    }

}

