package com.tdp2.utils.ui.openGraphView.tasks

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.text.TextUtils
import java.io.IOException
import java.net.URL
import java.util.concurrent.Callable

class ImageCallable(private val url: String) : Callable<Bitmap> {

    override fun call(): Bitmap? {
        if (TextUtils.isEmpty(url)) {
            return null
        }
        return try {
            val inputStream =
                URL(url).openStream()
            BitmapFactory.decodeStream(inputStream)
        } catch (e: IOException) {
            return null
        }
    }
}