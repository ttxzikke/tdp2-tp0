package com.tdp2.utils.ui.pageindicator

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import androidx.viewpager2.widget.ViewPager2
import com.tdp2.utils.R
import com.tdp2.utils.extensions.px
import kotlin.math.max

class PageIndicatorView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null
) : View(context, attrs) {

    companion object {
        val UNSELECTED_COLOR = Color.parseColor("#D8D8D8")
        val SELECTED_COLOR = Color.parseColor("#4A4A4A")
    }

    private var mCount = 0
    private var viewWidth = 0
    private var actualWidth = 0
    private var selectedPos = 0
    private val selectedPaint: Paint
    private val unselectedPaint: Paint
    private var dotMargin = 10.px
    private var selectedRadius = 5.px
    private var unselectedRadius = 5.px
    private val viewPaddingTop = 2.px
    private val viewPaddingBottom = 2.px

    var listener = object : ViewPager2.OnPageChangeCallback() {
        override fun onPageSelected(position: Int) {
            pageSelected(position)
        }
    }

    init {
        val a = context.obtainStyledAttributes(attrs, R.styleable.PageIndicatorView, 0, 0)
        selectedPaint = Paint()
        selectedPaint.isAntiAlias = true
        selectedPaint.color = a.getColor(
            R.styleable.PageIndicatorView_selectedColor,
            SELECTED_COLOR
        )
        selectedPaint.style = Paint.Style.FILL
        unselectedPaint = Paint(selectedPaint)
        unselectedPaint.color = a.getColor(
            R.styleable.PageIndicatorView_unselectedColor,
            UNSELECTED_COLOR
        )

        selectedRadius = a.getDimensionPixelSize(R.styleable.PageIndicatorView_selectedDotSize,selectedRadius)
        unselectedRadius = a.getDimensionPixelSize(R.styleable.PageIndicatorView_unselectedDotSize,unselectedRadius)
        dotMargin = a.getDimensionPixelSize(R.styleable.PageIndicatorView_dorMargin,dotMargin)
        a.recycle()
    }

    fun setViewPager(view: ViewPager2) {
        view.unregisterOnPageChangeCallback(listener)
        view.registerOnPageChangeCallback(listener)
        mCount = view.adapter?.itemCount ?: 0
        val maxRadius = max(selectedRadius, unselectedRadius)
        actualWidth = dotMargin * (mCount - 1) + maxRadius * 2 * mCount
        postInvalidate()
    }

    fun pageSelected(position: Int) {
        selectedPos = position
        postInvalidate()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        drawDots(canvas)
    }

    private fun drawDots(canvas: Canvas) {
        val maxRadius = max(selectedRadius, unselectedRadius)
        var x = (viewWidth - actualWidth) / 2 + maxRadius
        val y = (max(selectedRadius,unselectedRadius) * 2 + viewPaddingBottom + viewPaddingTop) / 2

        for (i in 0 until mCount) {
            val selected = selectedPos == i
            val radius = if (selected) selectedRadius else unselectedRadius
            canvas.drawCircle(
                x.toFloat(),
                y.toFloat(),
                radius.toFloat(),
                if (selected) selectedPaint else unselectedPaint
            )
            x += maxRadius * 2 + dotMargin
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        val mViewHeight = measureHeight(heightMeasureSpec)
        viewWidth = measureWidth(widthMeasureSpec)
        setMeasuredDimension(viewWidth, mViewHeight)
    }

    private fun measureWidth(measureSpec: Int): Int {
        val preferred = actualWidth
        return getMeasurement(measureSpec, preferred)
    }

    private fun measureHeight(measureSpec: Int): Int {
        val preferred = max(selectedRadius,unselectedRadius) * 2 + viewPaddingBottom + viewPaddingTop
        return getMeasurement(measureSpec, preferred)
    }

    private fun getMeasurement(measureSpec: Int, preferred: Int): Int {
        val specSize = MeasureSpec.getSize(measureSpec)
        val measurement: Int
        measurement = when (MeasureSpec.getMode(measureSpec)) {
            MeasureSpec.EXACTLY -> specSize
            MeasureSpec.AT_MOST -> Math.min(preferred, specSize)
            else -> preferred
        }
        return measurement
    }

}
