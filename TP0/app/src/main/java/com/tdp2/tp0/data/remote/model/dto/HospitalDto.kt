package com.tdp2.tp0.data.remote.model.dto

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class HospitalDto(
    @Json(name = "categorias") val categories: String
)