package com.tdp2.tp0.presentation.di.component

import com.tdp2.tp0.AppApplication
import com.tdp2.tp0.presentation.di.module.AppModule
import com.tdp2.tp0.presentation.di.module.ApplicationModule
import com.tdp2.tp0.presentation.di.module.DataModule
import com.tdp2.tp0.presentation.di.module.builder.ActivityBuilderModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ActivityBuilderModule::class,
        ApplicationModule::class,
        DataModule::class]
)
interface AppComponent : AndroidInjector<AppApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: AppApplication): Builder

        fun build(): AppComponent
    }

}